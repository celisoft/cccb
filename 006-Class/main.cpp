/**
 * This code is using C++11.
 * Compilation must be done with right options:
 * g++ -o test -std=c++11 -Wall main.cpp player.cpp
 */

#include <iostream>
#include "player.h"

using namespace std;

int main()
{
	Player lPlayer("celine");

	cout << "Starting score " << lPlayer.getScore() << endl;

	lPlayer.scoreUp();

	cout << "Updated score " << lPlayer.getScore() << endl; 

	return 0;
}
