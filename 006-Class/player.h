#ifndef PLAYER_H
#define PLAYER_H

#include <string>

using namespace std;

class Player
{
	string name;
	int score;

	public:
		//Constructor
		Player(string pName="anonymous")
		{
			name = pName;
			score = 0;
		}

		//Getters
		string getName(){ return name; }
		int getScore(){ return score; }

		//Setters
		void setName(string pName) { name = pName; }
		
		//Functions
		void scoreUp();
		void scoreDown();
};

#endif
