#include <iostream>
#include <string>
#include <sstream>
#include "guessit.h"

using namespace std;

//Get user input
int get_user_suggest()
{
	string input;
	int value;
	cout << "\nEnter a value (1-100):";
	getline(cin, input);
	stringstream(input) >> value;
	return value;
}

//Get user addiction
int get_user_addiction()
{
	string input;
	cout << "\nDo you want to play again ? (y/n):";
	getline(cin, input);
	if(input.compare("y") == 0)
	{
		return true;
	}
	return false;
}

//Init a game loop
void init_game_loop(int secret)
{
	int attempts{0};
	bool is_guessed = false;
	
	while(!is_guessed)
	{
		int guess = get_user_suggest();
		if(secret == guess)
		{
			is_guessed = true;
		}
		else
		{
			if(secret < guess)
			{
				cout << "The secret number is lower than " << guess << "." << endl;
			}
			else
			{
				cout << "The secret number is greater than " << guess << "." << endl;
			}
			attempts++;
		}		
	}

	cout << "You have found [" << secret << "] in " << attempts << " attempts !" << endl;
}

