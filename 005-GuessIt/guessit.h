#ifndef GUESSIT_H
#define GUESSIT_H

//Get user input
int get_user_suggest();

//Get user addiction
int get_user_addiction();

//Init a game loop
void init_game_loop(int secret);

#endif
