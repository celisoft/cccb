C++ come back
=============

Description
-----------

This repository is what I do to (re)learn C++ with the upgraded C++11 version.
You can freely use this code in order to refresh your memory too.
We all know that school time is a long long far away from us ;-).

Content details
---------------

- 001-HelloWorld is a classic hello world program.
- 002-DisplaySum displays the sum of 2 integers and shows some ways to initialize a var with auto or decltype.
- 003-CinStream uses users inputs to display some things.
- 004-FunctionalRiddle get a string decompose it through a function.
- 005-Guessit The guess a number game with a good random int generation + prepoc usage + header file. 
- 006-Class A (very) basic implementation of a Player class.
- 007-SDLTest consist in launching a SDL2 window 
- 008-SDL_MiniGame consist in a basic SDL2 game. Sprites came from Celisoft's [Pumpkin Obsession](http://www.celisoft.com/pumpkin_obsession). 

How to compile these code samples ?
-----------------------------------

For first examples, if you have g++, you can do this:
```
g++ -o exec_file -std=c++11 -Wall src_file
```

With clang++, you can do something like that:
```
clang++ -o exec_file -std=c++11 -stdlib=libstdc++ -Wall src_file
```

I have also tried to wrote g++ compilation line in the .cpp files
in order to compile it.

Makefiles will be done for too complex compilation process.

C++ resources
-------------

- [Official C++ website](http://www.cplusplus.com)
- [CPP reference](http://en.cppreference.com)
- [C++11 Cheat sheet](http://isocpp.org/blog/2012/12/c11-a-cheat-sheet-alex-sinyakov) by Alex Sinyakov
- The C++ programming language : a book written by Bjarne Stroustrup

Tools / library resources
-------------------------
- Managing Projects with GNU Make : a book written Robert Mecklenburg
- [SDL 1.2 to 2.0 migration guide](http://wiki.libsdl.org/MigrationGuide)
- [Official clang manual](http://clang.llvm.org/docs/UsersManual.html)

IDE warnings
------------

- Vim was used to create this code. Emacs is a good solution too.
- [Gnome Builder](https://wiki.gnome.org/Apps/Builder/) will be a very good graphic solution when it will be available.
- I'd like to have some feedbacks about Visual C++ for those Windows guys.
- Please avoid Eclipse, it's a JAVA IDE not a C/C++ one.

