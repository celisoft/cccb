/**
 * This code is using C++11 uniform initialization.
 * Compilation must be done with right options.
 * Sample :
 * g++ -o summer -std=c++11 -Wall summer.cc
 */

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main()
{
	string name, input_fav;
	int fav{0};

	//Here is a simple way to get string
	cout << "Enter your name: ";
	getline(cin, name);

	//Iterate over chars that composed the name to spell it
	for(auto lChar : name)
	{
		cout << lChar << " ";
	}
	cout << "!";

	//Here is a way to get string and automatically coonvert it into an int
	cout << "Hi " << name << " ! What is your favorite number ?";
	getline(cin, input_fav);
	stringstream(input_fav) >> fav;

	//Check that a calculation could be done with our sstreamed var
	cout << "Do you know that " << fav << "*10 = " << fav*10 << " ?!?" << endl;

	return 0;
}
