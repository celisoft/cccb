#include "game_window.h"

#include <SDL2/SDL_image.h>

//Initialize an 800x600 SDL window
bool GameWindow::init()
{
	//Try to initailize all SDL component and check if it works
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		return false;
	}

	//Create a window and check if it works
	display = SDL_CreateWindow("SDL Minigame",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		       	800, 600, SDL_WINDOW_OPENGL);
	if(display == nullptr)
	{
		return false;
	}

	//Create the renderer
	renderer = SDL_CreateRenderer(display, -1, 0);
	if(renderer == nullptr)
	{
		return false;
	}

	return true;
}

bool GameWindow::run()
{
	is_running = init();
	if(is_running == false)
	{
		//Init phase fails
		return false;
	}

	SDL_Event lEvent;

	while(is_running)
	{		
		SDL_RenderClear(renderer);
		
		if(SDL_PollEvent(&lEvent))
		{
			on_event(&lEvent);
		}

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

		player.render(renderer);
		pumpkin_manager.render(renderer);

		bool isCollide = pumpkin_manager.check_collision(player.get_player_rect());
		if(isCollide)
		{
			player.increase_score();
			if(pumpkin_manager.remove_eaten_pumpkins() == 0)
			{
				is_running = false;
			}
		}

		SDL_RenderPresent(renderer);

		//Slow down cycles
		SDL_Delay(16);
	}

	SDL_DestroyWindow(display);
	SDL_Quit();
	return true;
}

//Handle SDL events 
void GameWindow::on_event(SDL_Event* pEvent)
{
	switch(pEvent->type)
	{
		case SDL_KEYDOWN:
			switch(pEvent->key.keysym.sym)
			{
				case SDLK_LEFT:
					player.move_x(-1);
					break;
				case SDLK_RIGHT:
					player.move_x(1);
					break;
				case SDLK_UP:
					player.move_y(-1);
					break;
				case SDLK_DOWN:
					player.move_y(1);
					break;
			}
			break;
		case SDL_QUIT:
			is_running = false;
			break;
	}
}	

