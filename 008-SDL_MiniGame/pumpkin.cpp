#include "pumpkin.h"

//Render the texture through given renderer
void Pumpkin::render(SDL_Renderer* pRenderer)
{
	pumpkin_texture = SDL_CreateTextureFromSurface(pRenderer, pumpkin_image);
	SDL_RenderCopy(pRenderer, pumpkin_texture, &sprite_rect, &pumpkin_rect);
}

