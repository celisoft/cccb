#include "pumpkin_manager.h"

//Render the texture through given renderer
void PumpkinManager::render(SDL_Renderer* pRenderer)
{
	for(auto lPumpkin : pumpkins)
	{
		lPumpkin.render(pRenderer);
	}
}

//Collision detection between player rect and all pumpkins
bool PumpkinManager::check_collision(SDL_Rect* pPlayer)
{
	int idx = 0;
	for(auto lPumpkin : pumpkins)
	{
		if(SDL_HasIntersection(pPlayer, lPumpkin.get_pumpkin_rect()))
		{
			eaten_pumpkin = idx;			
			return true;
		}
		else
		{
			++idx;
		}
	}
	return false;
}

//Remove eaten pumpkins
int PumpkinManager::remove_eaten_pumpkins()
{
	if(eaten_pumpkin >= 0)
	{
		pumpkins.erase(pumpkins.begin()+eaten_pumpkin);
	}
	return pumpkins.size();
}

