#include "player.h"

//Render the texture through given renderer
void Player::render(SDL_Renderer* pRenderer)
{
	player_texture = SDL_CreateTextureFromSurface(pRenderer, player_image);
	SDL_RenderCopy(pRenderer, player_texture, &sprite_rect, &player_rect);
}

//Move on X axis with a default step of 1
void Player::move_x(int step=1)
{
	pos.set_x(pos.get_x()+step);

	//Update player sprite
	if(step < 0)
	{
		if(sprite_rect.x == LEFT_POS_0)
		{
			sprite_rect.x = 128*LEFT_POS_1;
		}
		else
		{
			sprite_rect.x = 128*LEFT_POS_0;
		}
	}
	else
	{
		if(sprite_rect.x == RIGHT_POS_0)
		{
			sprite_rect.x = 128*RIGHT_POS_1;
		}
		else
		{
			sprite_rect.x = 128*RIGHT_POS_0;
		}
	}

	//Update player_rect with updated X
	player_rect.x = pos.get_x() * STEP_X;
}

//Move on Y axis with a default step of 1
void Player::move_y(int step=1)
{
	pos.set_y(pos.get_y()+step);

	//Update player sprite
	sprite_rect.x = 128*FRONT_POS;

	//Update player_rect with updated Y
	player_rect.y = pos.get_y() * STEP_Y;
}


