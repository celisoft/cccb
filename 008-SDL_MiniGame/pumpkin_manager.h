#ifndef PUMPKIN_MANAGER_H
#define PUMPKIN_MANAGER_H

#include "position.h"
#include "pumpkin.h"
#include <random>
#include <chrono>
#include <functional>

using namespace std;

class PumpkinManager
{
	vector<Pumpkin> pumpkins;
	int eaten_pumpkin;

	public:
		//Constructor
		PumpkinManager()
		{
			//Reserve the 3 pumpkins rooms
			pumpkins.reserve(3);

			eaten_pumpkin = -1;

			//Creates randomizers
			unsigned lSeed = chrono::system_clock::now().time_since_epoch().count();
			default_random_engine rand_generator(lSeed);
			uniform_int_distribution<int> distribution_x(0, 11);
			uniform_int_distribution<int> distribution_y(0, 7);
			auto x_gen = bind(distribution_x, rand_generator);
		       	auto y_gen = bind(distribution_y, rand_generator);	

			//Generate 3 pumpkins with randomized positions
			for(int i=0; i<3; ++i)
			{
				Position lPos = Position(x_gen(), y_gen());
				pumpkins.push_back(Pumpkin(lPos));		
			}
		}

		//Destructor
		~PumpkinManager()
		{
			pumpkins.clear();
		}

		//Render the texture through given renderer
		void render(SDL_Renderer* pRenderer);

		//Collision detection between player rect and all pumpkins
		bool check_collision(SDL_Rect* pPlayer);

		//Remove eaten pumpkins
		int remove_eaten_pumpkins();
};

#endif
