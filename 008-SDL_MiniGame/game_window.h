#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H

#include <SDL2/SDL.h>
#include "player.h"
#include "pumpkin_manager.h"

class GameWindow
{
 	bool is_running;   
	SDL_Window* display;
	SDL_Renderer* renderer;
	Player player;	
	PumpkinManager pumpkin_manager;

	public:
		//Constructor
		GameWindow()
		{
			display = nullptr;
			renderer = nullptr;
		}

		//Initialize the game display
		bool init();

		//Display the game
		bool run();

		//Catch game events
		void on_event(SDL_Event* event);
};

#endif
