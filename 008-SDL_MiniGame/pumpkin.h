#ifndef PUMPKIN_H
#define PUMPKIN_H

#include "position.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

//This class is not optimized because we will load an image and a
//texture at each pumpkin. It's just for demo purpose !
class Pumpkin
{
	Position pos;
	SDL_Surface* pumpkin_image;	
	SDL_Texture* pumpkin_texture;
	SDL_Rect sprite_rect;
	SDL_Rect pumpkin_rect;

	public:
		//Constructor
		Pumpkin(Position pPosition)
		{
			pos = pPosition;

			pumpkin_image = IMG_Load("assets/pumpkin.png");

			sprite_rect.w = 32;
			sprite_rect.h = 32;
			sprite_rect.x = 0;
			sprite_rect.y = 0;

			pumpkin_rect.w = 32;
			pumpkin_rect.h = 32;
			pumpkin_rect.x = pPosition.get_x()*64;
			pumpkin_rect.y = pPosition.get_y()*64;
		}

		//Destructor
		~Pumpkin()
		{
			SDL_DestroyTexture(pumpkin_texture);
		}

		//Getter for pumpkin_rect
		SDL_Rect* get_pumpkin_rect(){return &pumpkin_rect;}

		//Render the texture through given renderer
		void render(SDL_Renderer* pRenderer);
};

#endif
