/**
 * This code is using C++11 uniform initialization.
 * Compilation must be done with right options.
 * Sample :
 * g++ -o summer -std=c++11 -Wall summer.cc
 */

#include <iostream>
#include <string>

using namespace std;

int main()
{
	int a{10};
	int b{5};
	
	//Declare result of same type as a (int)
	//decltype(a) result;
	//result = a+b;

	//Set the type automatically from a+b sum value
	auto result = a+b;

	string msg{"The result is "};

	//Line break can be done through \n or endl.
	cout << msg << result << endl;

	return 0;
}
