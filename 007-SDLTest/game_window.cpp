#include "game_window.h"

//Initialize an 800x600 SDL window
bool GameWindow::init()
{
	//Try to initailize all SDL component and check if it works
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		return false;
	}

	//Create a window and check if it works
	display = SDL_CreateWindow("SDL test",
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		       	800, 600, SDL_WINDOW_OPENGL);
	if(display == nullptr)
	{
		return false;
	}

	//Create the renderer
	renderer = SDL_CreateRenderer(display, -1, 0);
	if(renderer == nullptr)
	{
		return false;
	}

	return true;
}

bool GameWindow::run()
{
	is_running = init();
	if(is_running == false)
	{
		//Init phase fails
		return false;
	}

	SDL_Event lEvent;

	while(is_running)
	{
		SDL_RenderClear(renderer);
		
		while(SDL_PollEvent(&lEvent))
		{
			on_event(&lEvent);
		}
		
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderPresent(renderer);
	}

	SDL_DestroyWindow(display);
	SDL_Quit();
	return true;
}

//Handle SDL events (in fact just the exit one)
void GameWindow::on_event(SDL_Event* pEvent)
{
	if(pEvent->type == SDL_QUIT)
	{
		is_running = false;
	}
}	

