#ifndef GAME_WINDOW_H
#define GAME_WINDOW_H

#include <SDL2/SDL.h>

class GameWindow
{
 	bool is_running;   
	SDL_Window* display;
	SDL_Renderer* renderer;
	
	public:
		//Constructor
		GameWindow()
		{
			display = nullptr;
		}

		bool init();
		bool run();
		void on_event(SDL_Event* event);
};

#endif
